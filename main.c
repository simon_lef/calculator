#include <stdio.h>

int main(int argc, char const *argv[])
{
    printf("Bienvenu dans la supercalculatrice 2.0 !!\n");
    printf("Veuillez choisir l'opération que vous voulez réaliser : \nTappez : \n\t1 pour l'addition\n\t2 pour la soustraction");
    printf("\n\t3 pour la multiplication\n\t4 pour la division\n");
    
    char inputValue;
    do{
        printf("Allez-y ! \n");
        scanf("%c",&inputValue);
    }while(!(inputValue >= '1' && inputValue <= '4'));

    return 0;
}
